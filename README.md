# generate-unique-hash-key

    install :
        sudo npm i generate-unique-hash-key --save

    require :
        var generateUniqueHashKey = require("generate-unique-hash-key");

    use in prehook :

    schema.pre("save", function (next) {
        var self = this;

        if (self.status == "Created" && !self["transHash"]) {
            let params = {
                paramsString: "string to check as duplicate",     // ex: self.amount + self.mobile
                orderType: "recharge",
                expiresIn: 3      // In minutes, same paramsString transaction not allowed until minutes expires
            };
            generateUniqueHashKey.generateUniqueCodeRecord(params, function(err,uniqueData) {
                if(err) next(new Error(err));
                else if (uniqueData.isProceed) {
                    self["transHash"] = uniqueData.uniqueCode;
                    next();
                }
                else next(new Error(uniqueData.message));
            }
        } else {
            next();
        }
    });


   ex: 
       let params = {
                paramsString: self.amount + self.mobile + self.
                orderType: "recharge",
                expiresIn: 3      // In minutes, same paramsString transaction not allowed until minutes expires
       };
         
     Here, next transaction with same amount and mobile number cannot be done until 3 min         







