var mongoose = require("mongoose");
let md5 = require("md5");
var definition = require("./uniqueTransactionCode.model").definition;
var schema = new mongoose.Schema(definition);
var model = mongoose.model("uniqueTransactionCodes", schema);
var url = process.env.MONGO_URL;
mongoose.connect(url);

schema.index({ hashParam: 1, createdAt: 1 });

let checkDuplicateRecord = (hashParam) => {
    return new Promise((resolve, reject) => {
        let endDate = new Date();
        let startDate = new Date((new Date().setMinutes(new Date().getMinutes() - 8)));
        let currentDate = new Date();
        currentDate.setSeconds(0);
        currentDate.setMilliseconds(0);
        currentDate = new Date(currentDate);

        let aggregate = [{
            "$match": {
                "hashParam": hashParam,
                "createdAt": { "$gte": startDate, "$lte": endDate }
            }
        },
        {
            $project: {
                uniqueCode: 1,
                loggedOn: 1,
                dateDifference: {
                    $subtract: [currentDate, { $multiply: ["$expiresIn", 60, 1000] }]
                }
            },
        },
        {
            $project: {
                uniqueCode: 1,
                dateDifference: 1,
                flag: {
                    "$gte": ["$dateDifference", "$loggedOn"]    // 5:59 , 
                }
            }
        }, {
            "$match": {
                "flag": false
            }
        }];

        model.find({ "hashParam": hashParam, "createdAt": { "$gte": startDate, "$lte": endDate } }).count().exec().then((records) => {

            if (records && records > 0) {
                model.aggregate(aggregate, (err, record) => {
                    if (err) reject(err);
                    else if (record && record.length) {
                        resolve({ isUnique: false, message: "Duplicate transaction, please wait few minutes and try again", uniqueCode: record[0].uniqueCode });
                    } else {
                        resolve({ isUnique: true, message: "Not a duplicate transaction" });
                    }
                });
            } else {
                resolve({ isUnique: true, message: "No duplicates found" });
            }
        }).catch(err => reject(err));
    });

};

let generateUniqueCodeRecord = (params, cb) => {

    let loggedOn = new Date();
    loggedOn.setSeconds(0);
    loggedOn.setMilliseconds(0);
    loggedOn = new Date(loggedOn);

    let uniqueCodeRecord = {
        stringParam: params.paramsString,
        orderType: params.orderType,
        hashParam: md5(params.paramsString),
        expiresIn: params.expiresIn,    // expires in minutes
        loggedOn: loggedOn,     
        createdAt: new Date(),
        uniqueCode: md5(params.paramsString + loggedOn)
    };

    checkDuplicateRecord(uniqueCodeRecord.hashParam).then((data) => { //
        if (data.isUnique) {
            model.create(uniqueCodeRecord, (err, doc) => {
                if (err) {
                    cb(err, null);
                } else {
                    cb(null, { "isProceed": true, "message": "Unique Transaction entry Created...", uniqueCode: uniqueCodeRecord.uniqueCode });
                }
            });
        } else {
            cb(null, { "isProceed": false, "message": "Sorry, Please try after " + params.expiresIn + " min...", uniqueCode: data.uniqueCode });
        }
    }).catch(err => {
        cb(err, null);
    });
};

module.exports = {
    generateUniqueCodeRecord: generateUniqueCodeRecord
};