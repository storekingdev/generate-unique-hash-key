let definition = {

    stringParam: { type: String },
    orderType: { type: String },
    hashParam: { type: String, required: true, index: true },
    expiresIn: { type: Number },    // In minutes
    loggedOn: { type: Date },
    uniqueCode: { type: String, required: true, index: true, unique: true },
    createdAt: { type: Date },
    lastUpdated: { type: Date },
    deleted: { type: Boolean, default: false }
};

module.exports.definition = definition;